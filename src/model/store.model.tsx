import {Tag} from "./tag.model";

export type Store = {
    id: number;
    name: string;
    opening_time: string;
    closing_time: string;
    adress: string;
    city: string
    tag: Tag;
    rating: number;
    price: number;
    remaining_baskets: number;
    background_image: string;
    main_image: string
    type: string
    alt_background: string,
    alt_main: string
}