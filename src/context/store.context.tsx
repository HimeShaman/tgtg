import {Store} from "../model/store.model";
import {createContext, FC, ReactNode, useEffect, useState} from "react";
import {getStores} from "../service/store.service";

export interface StoreContextValue {
    stores: Store[],
    findStores: (query: string) => void
}

type State = {
    status: "initial" | "loading" | "success" | "error"
    storeList: Store[]
    error: string | null
}

export const StoreContext = createContext<StoreContextValue | null>(null)

export const StoreProvider: FC<{ children: ReactNode }> = ({children}) => {
    const [state, setState] = useState<State>({
        status: "initial",
        storeList: [],
        error: null
    })

    function processSearch(query: string) {
        setState({...state, status: "loading"})
        getStores(query)
            .then((data) => {
                setState({status: "success", error: null, storeList: data});
            })
            .catch((error) => {
                setState({status: "error", error, storeList: []})
            })
    }

    useEffect(() => {
        processSearch("")
    }, [])

    return (
        <StoreContext.Provider
            value={{
                findStores: processSearch,
                stores: state.storeList
            }}
        >
            {children}
        </StoreContext.Provider>
    )
}

