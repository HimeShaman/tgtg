import {Store} from "../model/store.model";

export const getStores = async (query: string): Promise<Store[]> => {
    return fetch(`http://localhost:3001/store?q=${query}`)
        .then(response => response.json())
        .catch((error) => console.error(error))
}