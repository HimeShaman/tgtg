import React from "react";
import "./App.css";
import {StoreProvider} from "./context/store.context";
import {StoreList} from "./component/storeList/storeList.component";

function App() {
    return (
        <div className="App">
            <header className="my-11">
                <img src="https://toogoodtogo.fr/images/logo/econ-textless.svg" alt="logo" className="w-10 h-10 left-10 top-10 md:w-20 md:h-20 absolute md:left-72"/>
                <p className="text-6xl font-bold text-green-800">Too Good Too Go</p>
                <p className="uppercase text-xl text-green-300">Sauvez des repas, aidez la planète !</p>
            </header>
            <StoreProvider>
                <h2 className="text-2xl font-semibold justify-center text-green-900">A proximité</h2>
                <StoreList/>
            </StoreProvider>
        </div>
);
}

export default App;
