import {Tag} from "../../model/tag.model";
import React, {useContext} from "react";
import {StoreContext} from "../../context/store.context";

type StoreCardProps = {
    name: string,
    opening_time: string,
    closing_time: string,
    city: string,
    price: number,
    remaining_baskets: number,
    background_image: string,
    alt_background: string,
    main_image: string,
    alt_main: string
}

const StoreCard = ({name, opening_time, closing_time, city, price, remaining_baskets, background_image, main_image, alt_background, alt_main}: StoreCardProps) => {
    return (
        <div title={name} className="bg-white hover:bg-green-50 flex rounded-lg shadow flex-col h-72 m-1">
            <div className="relative flex justify-center items-center">
                <img src={main_image}
                     alt={alt_main}
                     className="bg-white top-20 h-20 w-20 flex absolute object-contain rounded-full z-20 "
                />
            </div>
            <div className="relative h-1/2 flex justify-center">
                <img
                    src={background_image}
                    alt={alt_background}
                    className="rounded-t-lg h-full w-full object-cover z-10"
                />
            </div>
            <div className="relative h-1/2 bottom-0 flex flex-col justify-center align-bottom">
                <h3 className="font-bold"><span title="boutique">{name}</span> - <span title="ville">{city}</span></h3>
                <div title="nombre de paniers" className="bg-blue-800 text-white font-semibold w-28 rounded justify-center self-center" >{remaining_baskets < 5 ? remaining_baskets : "5+"} à sauver</div>
                <p aria-labelledby="horaires" className="text-gray-600">
                    <span title="ouverture">{opening_time} - </span>
                    <span title="fermeture">{closing_time}</span>
                </p>
                <p title="prix" className="text-gray-600">{new Intl.NumberFormat("fr-FR", {
                    style: "currency",
                    currency: "EUR"
                }).format(price)}</p>
            </div>
        </div>
    )
}

export const StoreList = () => {
    const storeContext = useContext(StoreContext);
    const list = storeContext?.stores

    return (
        <div title="Liste des commercants" className="grid md:grid-cols-3 grid grid-cols-1 gap-3 m-5">
            {list?.map((storeItem) => (
                <StoreCard key={storeItem.id} name={storeItem.name}
                           opening_time={storeItem.opening_time} closing_time={storeItem.closing_time}
                           city={storeItem.city} price={storeItem.price}
                           remaining_baskets={storeItem.remaining_baskets} background_image={storeItem.background_image}
                           alt_background={storeItem.alt_background}
                           main_image={storeItem.main_image}
                           alt_main={storeItem.alt_main}/>
            ))}

            {list && list.length === 0 && <span>Il n'y a pas de magasins disponibles</span>

            }
        </div>
    )
}
